package org.tmatesoft.svn.util;

import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;

public class SVNTest {

    public static void main(String[] args) throws SVNException {

        final SVNURL url = SVNURL.parseURIEncoded("file://T:/repos2");
        final SVNRepository repos = SVNRepositoryFactory.create(url);
        try {
            repos.log(null, -1, 0, true, false, new ISVNLogEntryHandler() {
                @Override
                public void handleLogEntry(SVNLogEntry logEntry) throws SVNException {
                    System.out.println(logEntry.getRevision());
                }
            });
        } finally {
            repos.closeSession();
        }
    }
}
